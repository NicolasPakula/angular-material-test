import { Component, OnInit } from '@angular/core';
import { Tv } from '../shared/models/tv';
import { TvService } from '../shared/services/tv.service';

@Component({
  selector: 'app-trending-list',
  templateUrl: './trending-list.component.html',
  styleUrls: ['./trending-list.component.css']
})
export class TrendingListComponent implements OnInit {
  tvs:Tv[] = [];
  constructor(private tvService:TvService) { }

  ngOnInit(): void {
    this.tvService.getTrending().subscribe((tv:any)=>{
      this.tvs=tv.results;
    })
  }

}
