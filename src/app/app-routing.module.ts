import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TrendingListComponent } from './trending-list/trending-list.component';

const routes: Routes = [
  {path: "tv", component: TrendingListComponent, pathMatch: "full"},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
