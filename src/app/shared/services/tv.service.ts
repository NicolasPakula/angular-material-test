import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Tv } from '../models/tv';

@Injectable({
  providedIn: 'root'
})
export class TvService {

  constructor(private http: HttpClient) { }

  getTrending(): Observable<any> {
    return this.http.get<any>(`${environment.api_link}/trending/tv/day?${environment.api_key}`);
  }
}
