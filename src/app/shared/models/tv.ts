export interface Tv {
    "overview": string
    "first_air_date": string,
    "backdrop_path": string,
    "original_name": string,
    "original_language": string,
    "poster_path": string,
    "name": string,
    "id": number,
    "vote_average": number,
    "media_type": string
}
