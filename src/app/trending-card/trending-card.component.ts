import { Component, Input, OnInit } from '@angular/core';
import { Tv } from '../shared/models/tv';

@Component({
  selector: 'app-trending-card',
  templateUrl: './trending-card.component.html',
  styleUrls: ['./trending-card.component.css']
})
export class TrendingCardComponent implements OnInit {
  @Input() tv!:Tv;
  constructor() { }

  ngOnInit(): void {
  }

}
